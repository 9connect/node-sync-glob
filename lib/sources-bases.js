'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _isGlob = require('./is-glob');

var _isGlob2 = _interopRequireDefault(_isGlob);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var reDir = /\/|\\/;
var reDirAll = new RegExp(reDir.source, 'gm');

/**
 * Determine the base paths of `sources` like:
 * - **files:** `foo/bar.txt` -> `foo`
 * - **directories:** `foo/bar/` -> `foo/bar`
 * - **globs:** `foo/*` -> `foo`
 *
 * @param {string|Array.<string>} sources - One or more files, directors or glob patterns.
 * @returns {Array.<string>} - Returns the base paths of `sources`.
 */
var sourcesBases = function sourcesBases(sources) {
  if (!Array.isArray(sources)) {
    // eslint-disable-next-line no-param-reassign
    sources = [sources];
  }

  return sources.reduce(function (bases, pattern) {
    if (pattern.charAt(0) === '!') {
      return bases;
    }

    var index = (0, _isGlob2.default)(pattern);
    var foundGlob = index > -1;
    var isDir = void 0;

    if (index > -1) {
      var charBeforeGlob = pattern.charAt(index - 1);

      isDir = reDir.test(charBeforeGlob);
      // eslint-disable-next-line no-param-reassign
      pattern = pattern.substring(0, index);
    }

    if (pattern) {
      if (foundGlob && !isDir || !foundGlob && _fs2.default.statSync(pattern).isFile()) {
        // eslint-disable-next-line no-param-reassign
        pattern = _path2.default.dirname(pattern);
      } else if (reDir.test(pattern.charAt(pattern.length - 1))) {
        // eslint-disable-next-line no-param-reassign
        pattern = pattern.slice(0, -1);
      }
    }

    // eslint-disable-next-line no-param-reassign
    pattern = pattern.replace(reDirAll, _path2.default.sep);

    if (bases.indexOf(pattern) === -1) {
      bases.push(pattern);
    }

    return bases;
  }, []);
};

exports.default = sourcesBases;