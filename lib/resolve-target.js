'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var cwd = process.cwd();

var getBase = function getBase(source, bases) {
  return bases.filter(function (base) {
    return source.indexOf(base) !== -1;
  })
  // eslint-disable-next-line no-confusing-arrow
  .reduce(function (hit, base) {
    return hit.length > base.length ? hit : base;
  }, '');
};

/**
 * Determines the target structure by resolving a given `source` against a list of base paths.
 *
 * @param {Array.<string>} bases - An array of base paths.
 * @returns {ResolveTargetFunc} - Returns an `source` to `target` resolving function.
 */
var resolveTarget = function resolveTarget(bases) {
  return function (source, target) {
    var from = _path2.default.join(cwd, getBase(source, bases));

    return _path2.default.join(target, _path2.default.relative(from, source));
  };
};

exports.default = resolveTarget;

/**
 * A function which resolves a given `source` to a given `target` based on list of base paths.
 *
 * @typedef {function} ResolveTargetFunc
 * @param {string} source - A file or dir to be resolved against a list of base paths.
 * @param {string} target - A destination folder where to append the diff of `source` and `bases`.
 * @returns {string} - Returns an expanded `target`.
 */