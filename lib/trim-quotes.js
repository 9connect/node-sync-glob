'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
var rePreQuotes = /^['"]/;
var rePostQuotes = /['"]$/;
/**
 * Trim quotes of a given string.
 *
 * @param {string} str - A string.
 * @returns {string} - Returns `str`, but trimmed from quotes like `'`, `"`.
 */
var trimQuotes = function trimQuotes(str) {
  return str.replace(rePreQuotes, '').replace(rePostQuotes, '');
};

exports.default = trimQuotes;