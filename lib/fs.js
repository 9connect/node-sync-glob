'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.stat = exports.remove = exports.copyDir = exports.copyFile = undefined;

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _fsExtra = require('fs-extra');

var _fsExtra2 = _interopRequireDefault(_fsExtra);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _bluebird = require('bluebird');

var _bluebird2 = _interopRequireDefault(_bluebird);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_bluebird2.default.config({ cancellation: true });

var copy = (0, _bluebird.promisify)(_fsExtra2.default.copy);
var ensureDir = (0, _bluebird.promisify)(_fsExtra2.default.ensureDir);
var readFile = (0, _bluebird.promisify)(_fsExtra2.default.readFile);
var writeFile = (0, _bluebird.promisify)(_fsExtra2.default.writeFile);

/**
 * Copy file from `source` to `target`.
 *
 * @param {string} source - A file to be copied.
 * @param {string} target - A destination path where to copy.
 * @param {TransformFunc} [transform] - Optional transformation function.
 * @returns {Promise}
 */
var copyFile = exports.copyFile = function copyFile(source, target, transform) {
  return new _bluebird2.default(function (resolve) {
    if (transform) {
      resolve(readFile(source).then(function (file) {
        var transformed = transform(file, target);
        var isObject = (typeof transformed === 'undefined' ? 'undefined' : _typeof(transformed)) === 'object';
        var data = isObject && transformed.data || transformed;
        var newTarget = isObject && transformed.target || target;

        return ensureDir(_path2.default.dirname(newTarget)).then(function () {
          return writeFile(newTarget, data);
        });
      }));
    } else {
      resolve(ensureDir(_path2.default.dirname(target)).then(function () {
        return copy(source, target);
      }));
    }
  });
};

/**
 * Copy a directory from `source` to `target` (w/o contents).
 *
 * @param {string} source - A directory to be copied.
 * @param {string} target - A destination path where to copy.
 * @returns {Promise}
 */
var copyDir = exports.copyDir = function copyDir(source, target) {
  return ensureDir(target);
};

/**
 * Remove a file or directory.
 *
 * @param {string} fileordir - The file or directory to remove.
 * @returns {Promise}
 */
var remove = exports.remove = (0, _bluebird.promisify)(_fsExtra2.default.remove);

var stat = exports.stat = (0, _bluebird.promisify)(_fsExtra2.default.stat);

/**
 * A custom function which transforms a given `file` contents and/or `target`.
 *
 * @typedef {function} TransformFunc
 * @param {File} file - A file object obtained by `fs.readFile`.
 * @param {string} target - The destination where to copy this `file`.
 * @returns {File|{data: File, target: string}} - Returns the transformed `file` and/or renamed `target`.
 */