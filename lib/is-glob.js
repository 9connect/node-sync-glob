"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
var reGlobFirstChar = /^\{.*[^\\]}|^\*\*?|^\[.*[^\\]]|^[!?+*@]\(.*[^\\]\)|^\?|^!(?=[^([])(?=[^\\][^(])/;
var reGlob = /(?!\\).(:?\{.*[^\\]}|\*\*?|\[.*[^\\]]|[!?+*@]\(.*[^\\]\)|\?)|^!(?=[^([])(?=[^\\][^(])/;

/**
 * Determines whether a provided string contains a glob pattern.
 *
 * @param {string} str - The string to test for glob patterns.
 * @returns {number} - Returns the index of the first glob pattern or `-1` if it is not a glob.
 */
var isGlob = function isGlob(str) {
  var match = reGlob.exec(str);
  var matchFirst = void 0;
  var index = match ? match.index : -1;

  if (!match || index === 0) {
    matchFirst = reGlobFirstChar.exec(str);

    if (matchFirst) {
      index = matchFirst.index;
    }
  }

  if ((index > 0 || index === 0) && !matchFirst) {
    // eslint-disable-next-line no-plusplus
    ++index;
  }

  return index;
};

exports.default = isGlob;