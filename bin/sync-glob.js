#!/usr/bin/env node
'use strict';

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _chalk = require('chalk');

var _chalk2 = _interopRequireDefault(_chalk);

var _yargs = require('yargs');

var _yargs2 = _interopRequireDefault(_yargs);

var _index = require('../index');

var _index2 = _interopRequireDefault(_index);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/* globals process */

var argv = _yargs2.default.usage('Usage: $0 <sources> <target>').boolean('copy-initial').default('copy-initial', false).describe('copy-initial', 'Copy at the initial time of watch.').boolean('delete').alias('d', 'delete').default('delete', true).describe('delete', 'Delete extraneous files from target').boolean('watch').alias('w', 'watch').default('watch', false).describe('watch', 'Watch changes in sources and keep target in sync').number('depth').alias('i', 'depth').default('depth', Infinity).describe('depth', 'Maximum depth if you have performance issues (not everywhere yet: only on existing mirrors and watch scenario)').boolean('dot').default('dot', false).describe('dot', 'Include dotfiles to copy.').string('transform').alias('t', 'transform').describe('transform', 'A module name to transform each file. sync-glob lookups the specified name via "require()".').alias('e', 'exit-on-error').default('exit-on-error', true).describe('exit-on-error', 'Exit if an error occurred').boolean('verbose').alias('v', 'verbose').default('verbose', false).describe('verbose', 'Moar output').alias('s', 'silent').default('silent', false).describe('silent', 'No output (except errors)').version().help('help').showHelpOnFail(false, 'Specify --help for available options').epilog('copyright 2016').command('sources', 'One or more globs, files or directories to be mirrored (glob exclusions are supported as well - ! prefix)', { alias: 'sources' }).command('target', 'Destination folder for mirrored files', { alias: 'target' }).demand(2).argv;
var _ = argv._;
var length = _.length;

if (length < 2) {
  // eslint-disable-next-line no-console
  console.error(_chalk2.default.bold.red('Expects exactly two arguments, received ' + length));
  process.exit(1);
}

if (argv.silent) {
  // eslint-disable-next-line no-console
  console.log = function () {};
}

var root = process.cwd();
var target = _.pop();
var sources = _;
var notifyPriority = {
  'error': 'high',
  'copy': 'normal',
  'remove': 'normal',
  'watch': 'normal',
  'max-depth': 'low',
  'no-delete': 'low'
};

var close = (0, _index2.default)(sources, target, {
  watch: argv.watch,
  copyInitial: argv.copyInitial,
  delete: argv.delete,
  depth: argv.depth || Infinity,
  dot: argv.dot,
  transform: argv.transform
}, function (event, data) {
  var priority = notifyPriority[event] || 'low';

  if (!argv.verbose && priority === 'low') {
    return;
  }

  switch (event) {

    case 'error':
      // eslint-disable-next-line no-console
      console.error('%s %s', _chalk2.default.bold('ERROR'), _chalk2.default.bold.red(data.message || data));

      if (argv.exitOnError) {
        if (typeof close === 'function') {
          close();
        }

        process.exit(1);
      }
      break;

    case 'copy':
      // eslint-disable-next-line no-console
      console.log('%s %s to %s', _chalk2.default.bold('COPY'), _chalk2.default.yellow(_path2.default.relative(root, data[0])), _chalk2.default.yellow(_path2.default.relative(root, data[1])));
      break;

    case 'mirror':
      // eslint-disable-next-line no-console
      console.log('%s %s to %s', _chalk2.default.bold('MIRROR'), _chalk2.default.green(data[0]), _chalk2.default.green(data[1]));
      break;

    case 'remove':
      // eslint-disable-next-line no-console
      console.log('%s %s', _chalk2.default.bold('DELETE'), _chalk2.default.yellow(_path2.default.relative(root, data[1] || data[0])));
      break;

    case 'watch':
      // eslint-disable-next-line no-console
      console.log('%s %s', _chalk2.default.bold('WATCHING'), _chalk2.default.yellow(data));
      break;

    case 'max-depth':
      // eslint-disable-next-line no-console
      console.log('%s: %s too deep', _chalk2.default.bold.dim('MAX-DEPTH'), _chalk2.default.yellow(_path2.default.relative(root, data)));
      break;

    case 'no-delete':
      // eslint-disable-next-line no-console
      console.log('%s: %s extraneous but not deleted (use %s)', _chalk2.default.bold.dim('IGNORED'), _chalk2.default.yellow(_path2.default.relative(root, data)), _chalk2.default.blue('--delete'));
      break;

    // Fallback: forgotten logs, displayed only in verbose mode
    default:
      if (argv.verbose) {
        // eslint-disable-next-line no-console
        console.log('%s: %s', _chalk2.default.bold(event), data);
      }
  }
});